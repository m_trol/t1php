<?php

declare(strict_types=1);

namespace App\Task2;

class EmojiGenerator
{
    private $emoji=['🚀', '🚃', '🚄', '🚅', '🚇'];

    public function generate(): \Generator
    {
        foreach ($this->emoji as $val){

            yield $val;
        }
    }
}
