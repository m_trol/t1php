<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $fighters = [];

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;

    }

    public function mostPowerful(): Fighter
    {
        $mostPowerFighter = 0;

        for ($i = 1; $i < count($this->fighters); $i++) {

            if ($this->fighters[$i]->getAttack() > $this->fighters[$i-1]->getAttack() && $this->fighters[$i]->getAttack() > $this->fighters[$mostPowerFighter]->getAttack()) {

                $mostPowerFighter = $i;

            } elseif ($this->fighters[$i-1]->getAttack() > $this->fighters[$mostPowerFighter]->getAttack()) {

                $mostPowerFighter = $i-1;
            }

        }

        return $this->fighters[$mostPowerFighter];
    }

    public function mostHealthy(): Fighter
    {
        $mostHealthyFighter = 0;

        for ($i = 1; $i < count($this->fighters); $i++) {

            if ($this->fighters[$i]->getHealth() > $this->fighters[$i-1]->getHealth() && $this->fighters[$i]->getHealth() > $this->fighters[$mostHealthyFighter]->getHealth()) {
                $mostHealthyFighter = $i;
            } elseif ($this->fighters[$i-1]->getHealth() > $this->fighters[$mostHealthyFighter]->getHealth()) {
                $mostHealthyFighter = $i-1;
            }
        }

        return $this->fighters[$mostHealthyFighter];
    }

    public function all(): array
    {
        return $this->fighters;
    }

}
