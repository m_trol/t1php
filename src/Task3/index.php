<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task1\FightArena;
use App\Task3\FightArenaHtmlPresenter;
use App\Task1\Fighter;

$arena = new FightArena();
$presenter = new FightArenaHtmlPresenter();


for ($i = 0 ; $i <= 2; $i++) {
    $arena->add(defaultFighter($i));
}

$presentation = $presenter->present($arena);

function defaultFighter(int $i)
{
    list($id, $name, $health, $attack, $image)=(defaultFightersDataProvider()[$i]);
    return new Fighter($id, $name, $health, $attack, $image);
}

function defaultFightersDataProvider(): array
{
    return [
        [
            1,
            'Ryu',
            100,
            10,
            'https://bit.ly/2E5Pouh'
        ],
        [
            2,
            'Chun-Li',
            70,
            30,
            'https://bit.ly/2Vie3lf'
        ],
        [
            3,
            'Ken Masters',
            80,
            20,
            'https://bit.ly/2VZ2tQd'
        ]
    ];
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style type="text/css">
        img{
            height: 350px;
            width: auto;
        }
        .f{
            width: 350px;
            height: 380px;
            padding: 10px;
            float: left;
            text-align: center;
        }
    </style>
    <title>Built-in Web Server</title>
</head>
<body>
      <?php echo $presentation; ?>
</body>
</html>
