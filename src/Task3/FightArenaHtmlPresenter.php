<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $htmlPresent='';

        foreach ($arena->all() as $val)
        {
           $htmlPresent.= '<div class="f"><span>' . $val->getName() . ': ' . $val->getHealth() .
                            ', ' . $val->getAttack() .'</span>'.
                            '<img src="' . $val->getImage() . '"></div>';
        }

        return $htmlPresent;
    }
}
